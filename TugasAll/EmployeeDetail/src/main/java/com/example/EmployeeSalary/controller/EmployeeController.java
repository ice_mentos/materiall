package com.example.EmployeeSalary.controller;

import com.example.EmployeeSalary.dto.EmployeeRequest;
import com.example.EmployeeSalary.dto.EmployeeResponse;
import com.example.EmployeeSalary.entity.Employee;
import com.example.EmployeeSalary.repository.EmployeeRepository;
import com.example.EmployeeSalary.repository.SalaryRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Entity;
import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private SalaryRepository salaryRepository;

    @PostMapping("/employee")
    public Employee saveEmployee(@RequestBody EmployeeRequest request) {
        return employeeRepository.save(request.getEmployee());
    }

    @GetMapping("/getEmployee")
    public List<EmployeeResponse> getEmployee(){
        return employeeRepository.getJoin();
    }
}
