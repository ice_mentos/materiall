package com.example.EmployeeSalary.repository;

import com.example.EmployeeSalary.entity.Salary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalaryRepository extends JpaRepository<Salary, Integer> {
}
