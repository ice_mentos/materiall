package com.example.EmployeeSalary.dto;

import com.example.EmployeeSalary.entity.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.EnumMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRequest {
    private Employee employee;

    public Employee getEmployee(){
        return employee;
    }
}
