package com.nexsoft;

public class MyCar extends Vehicle {
    private int carTotal;

    MyCar(int initTotal) {
        super(initTotal);
        this.carTotal = initTotal;
    }

    public void detail() {
        MyCar myNewCar = new MyCar(this.carTotal);
        // myNewCar.type();
        // super.fuel();
        // myNewCar.fuel();
        // myNewCar.color();
        System.out.println("Total car: " + myNewCar.getTotal());
        // logic
        // myNewCar.setTotal(10);
        // System.out.println("Total car: " + myNewCar.getTotal());
    }

    public void fuel() {
        System.out.println("Fuel: solar");
    }

    public void color() {
        System.out.println("Color: Black");
    }
}