package com.nexsoft;

import java.util.ArrayList;

import com.nexsoft.detail.Parking;

/**
 * Hello world!
 *
 */
public class App extends Parking{
    public static void main(String[] args) {

        App detailApp = new App();
        detailApp.detail();
        detailApp.secondRun();

        Parking parkingClass = new Parking();
        Parking parkingClass2 = new Parking();
        parkingClass.detail();
        // parkingClass.secondRun();

        System.out.println("Text: " + parkingClass.parkingLot);

        int newParkingNo = parkingClass.setParkingNo(8);
        System.out.println("New Parking no: " + newParkingNo);


        int realParkingNo = parkingClass.newLot();
        System.out.println("New lot: " + realParkingNo);

        int realParkingNo2 = parkingClass2.newLot();
        System.out.println("New lot: " + realParkingNo2);

        int realParkingNo3 = detailApp.newLot();
        System.out.println("New lot: " + realParkingNo3);

        // firstRun();

        // System.out.println("Hello World!");

        // // Vehicle myCar = new Vehicle();
        // // myCar.type();
        // // myCar.fuel();
        
        // ArrayList<String> karyawan = new ArrayList<String>();
        // karyawan.add("Employee 1");
        // karyawan.add("Employee 2");
        // karyawan.add("Employee 3");

        // System.out.println("Nama: " + karyawan.get(1));
        // System.out.println("Banyak karyawan/ti: " + 
        // karyawan.size());

        // for (String karya : karyawan) {
        //     System.out.println("Nama: " + karya);
        // }

        // String[] employees = { "Karyawan 1", "Karyawati 1", "Karyawati 2" };

        // // employees[4] = "asdf";

        // System.out.println("Nama: " + employees[2]);
        // System.out.println("Banyak karyawan/ti: " + employees.length);

        // for (String employee : employees) {
        //     System.out.println("Nama: " + employee);
        // }

        // for (int i = 1; i <= 5; i++) {
        //     System.out.println("No: " + i);

        //     MyCar myNewCar = new MyCar(i);
        //     myNewCar.detail();
        // }

        // MyCar myNewCar = new MyCar(3);
        // myNewCar.detail();

        // Car myDetail = new Car();
        // myDetail.transmission();
    }

    public static void firstRun() {
        System.out.println("Main first run");
    }

    public void secondRun() {
        System.out.println("Main second run");
    }

    public int newLot() {
        return 15;
    }
}

// Employee:
// - Id Card (id card no, name)
// - Parking Card (id parking card, id card no)
// - Salary (id card no, salary)

// 1. Buat 5 Pegawai yang memiliki nama dan no id card yang berbeda:
// - 3 karyawan, 2 Manager
// - salary default karyawan 3jt
// - salary default manager 10jt
// 2. Buat Output untuk menampilkan
// - [ Parking card id - Nama ]
// - [ Nama - Salary] // salary = salary default * id card no