package com.nexsoft;

public class Vehicle {
    private int total;

    Vehicle(int initTotal) {
        // this = Vehicle
        this.total = initTotal;
    }

    public void type() {
        System.out.println("Type: Bus");
    }

    public void fuel() {
        System.out.println("Fuel: Gasoline");
    }

    public void setTotal(int newTotal) {
        this.total = newTotal;
    }

    public int getTotal() {
        return this.total;
    }
    
}