package org.example;
import org.json.JSONObject;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        App myClass = new App();
        myClass.parse();
    }
    public void parse() {

        JSONObject myObject = new JSONObject();
        myObject.put("name", "Admin");
        System.out.println(myObject.toString());

        String jsonString = "{\"name\": \"admin\"}";
        JSONObject myresp = new JSONObject(jsonString);
        System.out.println(myresp.getString("name"));
    }
}
