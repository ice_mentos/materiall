package com.example.demo.service;

import com.example.demo.entity.Employee;
import com.example.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository repository;

    public Employee saveEmployee(Employee employee){
        return repository.save(employee);
    }

    public List<Employee> saveEmployees(List<Employee> employees){
        return repository.saveAll(employees);
    }

    public List<Employee> getEmployees(){
        return  repository.findAll();
    }

    public Employee getEmployeeByNik(int nik){
        return repository.findById(nik).orElse(null);
    }

    public Employee getEmployeeByName(String name){
        return repository.findByName(name);
    }

    public String deleteEmployee(int nik){
        repository.deleteById(nik);
        return "Employee removed!!";
    }

//    public Employee updateEmployee(Employee employee) {
//        Employee existingEmployee = repository.findById(employee.getId()).orElse(null);
//        existingEmployee.setNik(employee.getNik());
//        existingEmployee.setNama(employee.getNama());
//        existingEmployee.setTempat(employee.getTempat());
//        existingEmployee.setTanggalLahir(employee.getTanggal_lahir());
//        return repository.save(existingEmployee);
//    }
}
