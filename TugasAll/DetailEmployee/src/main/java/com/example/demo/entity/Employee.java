package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Employee {
    @Id
    @GeneratedValue
    private int nik;
    private String name;
    private String tempat_tgllahir;
    private String jenis_kelamin;
    private String gol_darah;
    private String alamat;
    private String rt_rw;
    private String kelurahan;
    private String kecamatan;
    private String agama;
    private String status;
    private String pekerjaan;
    private String kewarganegaraan;
}
