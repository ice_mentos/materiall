package com.nexsoft.springboot.repository;

import com.nexsoft.springboot.entity.Salary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GajiRepository extends JpaRepository<Salary, Integer> {
}
