package com.nexsoft.springboot.dto;

import com.nexsoft.springboot.repository.EmpRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@ToString

public class GajiRequest {

    private EmpRepository Employee;

}
