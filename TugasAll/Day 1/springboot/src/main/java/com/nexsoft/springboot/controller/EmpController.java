package com.nexsoft.springboot.controller;

import com.nexsoft.springboot.dto.GajiResponse;
import com.nexsoft.springboot.entity.Employee;
import com.nexsoft.springboot.repository.EmpRepository;
import com.nexsoft.springboot.repository.GajiRepository;
import com.nexsoft.springboot.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class EmpController {
    @Autowired
    private EmpService service;

    @Autowired
    private EmpRepository empRepository;

    @Autowired
    private GajiRepository gajiRepository;

    @PostMapping("/addEmployee")
    public Employee addEmployee(@RequestBody Employee employee) {
        return service.saveEmployee(employee);
    }

    @PostMapping("/addEmployees")
    public List<Employee> addEmployees(@RequestBody List<Employee> employees) {
        return service.saveEmployees(employees);
    }

    @GetMapping("/employees")
    public List<Employee> findAllEmployees() {
        return service.getEmployees();
    }

    @GetMapping("/employee/{id}")
    public Employee findEmployeeById(@PathVariable int id){
        return service.getEmployeeById(id);
    }

    @GetMapping("/employeeByName/{name}")
    public Employee findEmployeeByName(@PathVariable String name){
        return service.getEmployeeByName(name);
    }

    @PutMapping("/update")
    public Employee updateEmployee(@RequestBody Employee employee){
    return service.saveEmployee(employee);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable int id){
        return service.deleteEmployee(id);
    }

    @GetMapping("/getSalary")
    public List<GajiResponse> getJoinInformation() {
        return empRepository.getJoinInformation();
    }

}
