package com.nexsoft.springboot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
//@ToString
@Table(name = "EMPLOYEE_MASTER")
public class Employee {
    @Id
    @GeneratedValue
    private int id;
    private String nik;
    private String name;
    private String pob;
    private String bd;
    private String gender;
    private String address;
    private String religion;
    private String profession;
    private String citizenship;
//
//    @OneToMany(targetEntity = Salary.class, cascade = CascadeType.ALL)
//    @JoinColumn(name = "kg_fk", referencedColumnName = "profession")
//    private List<Salary> salaries;


}
