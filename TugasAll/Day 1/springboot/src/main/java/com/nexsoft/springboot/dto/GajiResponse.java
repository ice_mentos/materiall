package com.nexsoft.springboot.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)

public class GajiResponse {

    private String name;
    private String profession;
    private String salary;
    public GajiResponse(String name, String profession, String salary) {
        this.name = name;
        this.profession = profession;
        this.salary = salary;
    }
    private int id;

}
