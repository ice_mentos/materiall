package com.nexsoft.springboot.repository;

import com.nexsoft.springboot.dto.GajiResponse;
import com.nexsoft.springboot.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmpRepository extends JpaRepository<Employee, Integer> {
    Employee findByName(String name);

    @Query("SELECT new com.nexsoft.springboot.dto.GajiResponse(k.name,k.profession, g.salary) FROM ToMany k JOIN k.salaries g WHERE k.profession = g.profession")

    public List<GajiResponse> getJoinInformation();
}