package com.nexsoft.springboot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@ToString
@Entity
@Table(name = "salary")
public class Salary {
    @Id
    private int id;
//    private String name;
    private String profession;
    private String salary;

}
