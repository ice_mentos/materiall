package org.example;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class SocketConn {
    String pesan;
    Boolean kondisi = true;
    public void clientConn() {
        while (kondisi) {
            try {
                pesan = System.console().readLine();
                Socket myConn = new Socket("10.10.1.135", 4444);
                DataOutputStream dataOutput = new DataOutputStream(myConn.getOutputStream());

                dataOutput.writeUTF("Gilas : " + pesan);
                dataOutput.flush();
                dataOutput.close();

                myConn.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (pesan.equalsIgnoreCase("bye")) {
                    kondisi = false;
                }
            }
        }
    }
}
