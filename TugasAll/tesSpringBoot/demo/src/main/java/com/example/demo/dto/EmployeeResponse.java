package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
// untuk meng ignore yang tidak ada di entity
@JsonIgnoreProperties(ignoreUnknown = true)

// json include, ketika di post akan di baca meskipun tidak ada entity
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class EmployeeResponse {
    private String name;
    private int salary;

}
