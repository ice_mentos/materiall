package com.example.demo.repository;

import com.example.demo.dto.EmployeeResponse;
import com.example.demo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
@Query(value = "Select new com.example.demo.dto.EmployeeResponse(a.name, b.salary) from Employee a join a.salaries b")

    public List<EmployeeResponse> getJoinInformation();

}
