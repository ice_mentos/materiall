package com.example.demo.dto;

import com.example.demo.entity.Karyawan;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JoinRequest {
    private Karyawan karyawan;

    public Karyawan getKaryawan(){ return karyawan; }
}
