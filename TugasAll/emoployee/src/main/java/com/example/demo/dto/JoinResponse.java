package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class JoinResponse {
    private long id;
    private String nama;
    private String gol;
    private String tempat;
    private String tanggal;
    private String jk;
    private String agama;
    private String status;
    private String pekerjaan;
    private String kwr;
    private String alamat;
    private String rt;
    private String rw;
    private String kel;
    private String kec;
    private String posisi;
    private long salary;
}
