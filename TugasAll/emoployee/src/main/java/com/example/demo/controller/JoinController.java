package com.example.demo.controller;

import com.example.demo.dto.JoinRequest;
import com.example.demo.dto.JoinResponse;
import com.example.demo.entity.Karyawan;
import com.example.demo.repository.KaryawanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class JoinController {
    @Autowired
    private KaryawanRepository repo;

    @PostMapping("/addKaryawan")
    public Karyawan saveKaryawan(@RequestBody JoinRequest request){
        return repo.save(request.getKaryawan());
    }

    @GetMapping("/getKaryawan")
    public List<JoinResponse> getKaryawan(){
        return repo.getAllData();
    }

    @GetMapping("/getKaryawanByName/{name}")
    public List<JoinResponse> getbyname(@PathVariable String name){
        return repo.getAllDataByName(name);
    }

    @GetMapping("/getKaryawanBySalary/{first}/{second}")
    public List<JoinResponse> getbysalary(@PathVariable Long first,@PathVariable Long second){
        return repo.getAllDataBySalary(first, second);
    }

    @GetMapping("/getKaryawanByJabatanAsc")
    public List<JoinResponse> getbyjabatanasc(){
        return repo.getAllDataPosisiAsc();
    }

    @GetMapping("/getKaryawanByJabatanDesc")
    public List<JoinResponse> getbyjabatandesc(){
        return repo.getAllDataPosisiDesc();
    }
}
