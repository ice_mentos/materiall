package com.example.EmployeeSalary2.dto;

import com.example.EmployeeSalary2.entity.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRequest {
    private Employee employee;

    public Employee getEmployee() {
        return employee;

    }
}