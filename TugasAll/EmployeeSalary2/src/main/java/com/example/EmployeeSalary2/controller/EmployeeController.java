package com.example.EmployeeSalary2.controller;

import com.example.EmployeeSalary2.dto.EmployeeRequest;
import com.example.EmployeeSalary2.dto.EmployeeResponse;
import com.example.EmployeeSalary2.entity.Employee;
import com.example.EmployeeSalary2.repository.EmployeeRepository;
import com.example.EmployeeSalary2.repository.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private SalaryRepository salaryRepository;

    @PostMapping("/employee")
    public Employee saveEmployee(@RequestBody EmployeeRequest request) {
        return employeeRepository.save(request.getEmployee());
    }

    @GetMapping("/getEmployee")
    public List<EmployeeResponse> getEmployee(){
        return employeeRepository.getJoin();
    }
}
