package com.example.EmployeeSalary2.repository;

import com.example.EmployeeSalary2.entity.Salary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalaryRepository extends JpaRepository<Salary, Integer> {
}
