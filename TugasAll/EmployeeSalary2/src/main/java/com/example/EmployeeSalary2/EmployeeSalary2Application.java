package com.example.EmployeeSalary2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeSalary2Application {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeSalary2Application.class, args);
	}

}
