package com.example.EmployeeSalary2.repository;

import com.example.EmployeeSalary2.dto.EmployeeResponse;
import com.example.EmployeeSalary2.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    @Query("SELECT new com.example.EmployeesSalary.dto.EmployeeResponse(e.name, s.salary) FROM Employee e JOIN e.salaries s")
    public List<EmployeeResponse> getJoin();
}
