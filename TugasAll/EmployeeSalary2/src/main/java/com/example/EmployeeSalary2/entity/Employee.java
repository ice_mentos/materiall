package com.example.EmployeeSalary2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Employee {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String gender;
    private String address;

    @OneToMany(targetEntity = Salary.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "eid", referencedColumnName = "id")
    private List<Salary> salaries;
}

