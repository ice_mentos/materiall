package org.example;

import org.example.connection.SocketConn;
import org.example.properties.ReadProp;
import org.example.properties.writeProp;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException {
        System.out.println( "Hello World!" );

        writeProp wProp = new writeProp();
        wProp.createProp();

//        ReadProp rProp = new ReadProp();
//        rProp.readProp();
//
//        SocketConn mServer = new SocketConn();
//        mServer.runServer(6666);

    }
}
