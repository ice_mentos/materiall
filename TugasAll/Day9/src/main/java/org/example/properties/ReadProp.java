package org.example.properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadProp {
    public void readProp(){
        Properties counf = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream("config.properties");

            counf.load(input);

            String server = counf.getProperty("server");
            String port = counf.getProperty("port");
            System.out.println("Server " + server);
            System.out.println("Port " + port);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (input != null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
