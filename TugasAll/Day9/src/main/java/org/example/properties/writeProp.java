package org.example.properties;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class writeProp {
    public void createProp() throws IOException {
        Properties counf = new Properties();
        OutputStream output = null;

        try {
            output = new FileOutputStream("config.properties");

            counf.setProperty("server", "10.10.1.135");
            counf.setProperty("port", 4444);

            counf.store(output, null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            if (output != null){
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
