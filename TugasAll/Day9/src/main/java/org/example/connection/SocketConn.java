package org.example.connection;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketConn {
    public void runServer(String server, int port){
        try {
            ServerSocket myServer = new ServerSocket(port);
            Socket myServerConn = myServer.accept();

            DataInputStream dataInput = new DataInputStream(myServerConn.getInputStream());
            String readData = dataInput.readUTF();

            System.out.println("Data : " + readData);

            myServer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
