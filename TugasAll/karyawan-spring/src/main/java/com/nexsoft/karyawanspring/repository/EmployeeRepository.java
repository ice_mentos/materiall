package com.nexsoft.karyawanspring.repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nexsoft.karyawanspring.entity.Employee;
import com.nexsoft.karyawanspring.entity.Join;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    @Query("select new com.nexsoft.karyawanspring.entity.Join(e.nik, e.alamat, e.nama, e.jenis_kelamin, j.jabatan, s.salary) from Employee e join e.position j join e.salary s")
    public List<Join> joinSalaryPosition();
}