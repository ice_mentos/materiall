package com.nexsoft.karyawanspring.repository;

import com.nexsoft.karyawanspring.entity.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository <Admin, Integer> {
}
