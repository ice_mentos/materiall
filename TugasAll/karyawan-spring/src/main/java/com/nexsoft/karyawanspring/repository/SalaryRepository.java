package com.nexsoft.karyawanspring.repository;

import com.nexsoft.karyawanspring.entity.Salary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalaryRepository extends JpaRepository<Salary, Integer> {
}
