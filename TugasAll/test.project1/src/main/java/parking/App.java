package parking;

public class App {
    public static void main(String[] args) {
        IdCard cardtest = new IdCard();

        cardtest.setData(1, "Budi", "Karyawan", 3);
        cardtest.showData();

        cardtest.setData(2, "Gilas", "Manager", 6);
        cardtest.showData();

        cardtest.setData(3, "Wahyu", "Karyawan", 9);
        cardtest.showData();

        cardtest.setData(4, "Bagus", "Karyawan", 12);
        cardtest.showData();

        cardtest.setData(5, "Kiki", "Manager", 15);
        cardtest.showData();
}


}


