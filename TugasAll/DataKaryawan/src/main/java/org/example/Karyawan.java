package org.example;

import org.json.JSONObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Karyawan {
        ConnectionSql connection = new ConnectionSql().Connect();
        ArrayList<String> datas = new ArrayList<>();

        void showData() throws SQLException {
                String query = "SELECT * FROM data";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                ResultSet resultSet = preparedStatement.executeQuery(query);
                while (resultSet.next()){
                        int id = resultSet.getInt("id");
                        String nama = resultSet.getString("nama");
                        String tempat = resultSet.getString("tempat");
                        String tanggalLahir = resultSet.getString("tanggal_lahir");
                        String jenisKelamin = resultSet.getString("jenis_kelamin");

                        String response = "{\"id\" : \""+ id +"\", \"nama\" : \""+ nama +"\", \"tempat\" : \""+ tempat +"\", \"tanggal_lahir\" : \""+ tanggalLahir +"\", \"jenis_kelamin\" : \""+ jenisKelamin +"\"}";
//            System.out.println(response);
                        datas.add(response);
                }
        }

        void addData(String theJSON) throws SQLException {
                JSONObject theResponse = new JSONObject(theJSON);
                if (theResponse.getString("type").equalsIgnoreCase("data")){
                        String nama = theResponse.getString("nama");
                        String tempat = theResponse.getString("tempat");
                        String tanggalLahir = theResponse.getString("tanggal_lahir");
                        String jenisKelamin = theResponse.getString("jenis_kelamin");
                        String alamat = theResponse.getString("alamat");

                        DateTimeFormatter myParse = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        LocalDate Parser = LocalDate.parse(tanggalLahir, myParse);

                        DateTimeFormatter FormatDate = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                        String tglLahir = Parser.format(FormatDate);

                        String query = "INSERT INTO data(nama, tempat, tanggal_lahir, jenis_kelamin) VALUES ('"+ nama +"', '"+ tempatLahir +"', '"+ tglLahir +"', '"+ jenisKelamin +"')";
//            System.out.println(query);
                        PreparedStatement preparedStatement = connection.prepareStatement(query);
                        preparedStatement.executeUpdate(query);
                }
        }

        ArrayList<String> getDatas() throws SQLException {
                showData();
                return datas;
        }
}
