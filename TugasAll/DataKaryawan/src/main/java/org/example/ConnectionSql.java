package org.example;
//package org.example.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionSql {
    static String db_url = "jdbc:mysql://localhost:3306/karyawan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    ConnectionSql connection;

    public ConnectionSql Connect(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(db_url, "root", "");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }
}
