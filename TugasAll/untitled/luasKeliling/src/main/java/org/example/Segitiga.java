package org.example;

import java.util.Scanner;

public class Segitiga {
    public void showSegitiga() {
        Scanner input = new Scanner(System.in);
        int alas,tinggi,miring;
        double luas,keliing;

        System.out.println("Luas & keliling Segitiga Siku-siku");
        System.out.print("Alas : ");
        alas = input.nextInt();

        System.out.print("Tinggi : ");
        tinggi = input.nextInt();

        System.out.print("Miring : ");
        miring = input.nextInt();

        luas = alas*tinggi/2;
        keliing = alas+tinggi+miring;

        System.out.println("");
        System.out.println("Luas Segitiga : " + luas);
        System.out.println("Keliling Segitiga : " + keliing);

    }
}
