package com.nexsoft;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        ArrayList<String> collection1 = new ArrayList<>();
        collection1.add("Risa");
        collection1.add("Reza");
//        collection1.add("Liana");
        collection1.add("Ong");

        ArrayList<String> collection2 = new ArrayList<>();
        collection2.add("Anggi");
        collection2.add("Liana");
//        collection2.add("Ong");

        System.out.println("Length: "+collection1.size());

        if(!collection1.contains("Anggi")) {
            collection1.add(0, "Anggi");
        }
        System.out.println("Length: "+collection1.size());
        System.out.println("Empty: "+collection1.isEmpty());
//        collection1.clear();
        System.out.println("Length: "+collection1.size());
        System.out.println("Empty: "+collection1.isEmpty());

        if(collection1.isEmpty()) {
            System.out.println("Data is empty!");
        }

        for(String name : collection1) {
            System.out.println("Nama: "+name);
        }

        System.out.println("Remove 1:"+collection1.remove("Reza"));
        System.out.println("Remove 2:"+collection1.remove(1));

        System.out.println("========= Iterator ==========");
        System.out.println(collection1.retainAll(collection2));
        Iterator itrCollection1 = collection1.iterator();
        while (itrCollection1.hasNext()) {
            System.out.println("Name: "+ itrCollection1.next());
        }

        System.out.println(collection1.equals(collection2));
        System.out.println("containsAll: "+collection1.containsAll(collection2));
        System.out.println(collection1.get(0));

        ArrayList<String> goods = new ArrayList<>();
        goods.add("Indomie");
        goods.add("Kapal Api");
        goods.add("Popok Bayi");
        goods.add("Shampoo");
        goods.add("Sabun Mandi");

        ArrayList<String> goodsNew = new ArrayList<>();
        goodsNew.add("Indomie");
        goodsNew.add("Kapal Api");
        goodsNew.add("Baygon");

        goods.addAll(goodsNew);

        System.out.println(goods.size());

    }
}
