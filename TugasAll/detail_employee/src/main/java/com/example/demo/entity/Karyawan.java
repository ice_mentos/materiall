package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "karyawan")
public class Karyawan {
    @Id
    @GeneratedValue
    private long id;
    private String nama;
    private String gol;
    private String tempat;
    private String tanggal;
    private String jk;
    private String agama;
    private String status;
    private String pekerjaan;
    private String kwr;
    private String alamat;
    private String rt;
    private String rw;
    private String kel;
    private String kec;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "fkposisi", referencedColumnName = "id")
    private Posisi posisi;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "fksalary", referencedColumnName = "id")
    private Salary salary;

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }



}
