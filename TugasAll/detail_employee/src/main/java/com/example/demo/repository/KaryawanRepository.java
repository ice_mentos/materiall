package com.example.demo.repository;

import com.example.demo.dto.JoinResponse;
import com.example.demo.entity.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface KaryawanRepository extends JpaRepository<Karyawan, Long> {

    @Query("SELECT new com.example.demo.dto.JoinResponse(a.id, a.nama, a.gol, a.tempat, a.tanggal, a.jk, " +
            "a.agama, a.status, a.pekerjaan, a.kwr, a.alamat, a.rt, a.rw, a.kel, a.kec, b.posisi, c.salary) " +
            "FROM Karyawan a JOIN a.posisi b JOIN a.salary c")
    public List<JoinResponse> getAllData();

    @Query("SELECT new com.example.demo.dto.JoinResponse(a.id, a.nama, a.gol, a.tempat, a.tanggal, a.jk, " +
            "a.agama, a.status, a.pekerjaan, a.kwr, a.alamat, a.rt, a.rw, a.kel, a.kec, b.posisi, c.salary) " +
            "FROM Karyawan a JOIN a.posisi b JOIN a.salary c WHERE a.nama LIKE %:nama%")
    public List<JoinResponse> getAllDataByName(@Param("nama") String nama );

    @Query("SELECT new com.example.demo.dto.JoinResponse(a.id, a.nama, a.gol, a.tempat, a.tanggal, a.jk, " +
            "a.agama, a.status, a.pekerjaan, a.kwr, a.alamat, a.rt, a.rw, a.kel, a.kec, b.posisi, c.salary) " +
            "FROM Karyawan a JOIN a.posisi b JOIN a.salary c WHERE c.salary >= :first AND c.salary <= :second")
    public List<JoinResponse> getAllDataBySalary(@Param("first") Long first,
                                                 @Param("second") Long second);

    @Query("SELECT new com.example.demo.dto.JoinResponse(a.id, a.nama, a.gol, a.tempat, a.tanggal, a.jk, " +
            "a.agama, a.status, a.pekerjaan, a.kwr, a.alamat, a.rt, a.rw, a.kel, a.kec, b.posisi, c.salary) " +
            "FROM Karyawan a JOIN a.posisi b JOIN a.salary c ORDER BY b.posisi ASC")
    public List<JoinResponse> getAllDataPosisiAsc();

    @Query("SELECT new com.example.demo.dto.JoinResponse(a.id, a.nama, a.gol, a.tempat, a.tanggal, a.jk, " +
            "a.agama, a.status, a.pekerjaan, a.kwr, a.alamat, a.rt, a.rw, a.kel, a.kec, b.posisi, c.salary) " +
            "FROM Karyawan a JOIN a.posisi b JOIN a.salary c ORDER BY b.posisi DESC")
    public List<JoinResponse> getAllDataPosisiDesc();
}
