package com.example.demo.controller;


import com.example.demo.dto.EmployeeRequest;
import com.example.demo.dto.EmployeeResponse;
import com.example.demo.entity.Employee;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.repository.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class EmployeeDetailController {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private SalaryRepository salaryRepository;

    @PostMapping("/employee")
    public Employee saveEmployee(@RequestBody EmployeeRequest request) {
        return employeeRepository.save(request.getEmployee());
    }

    @GetMapping("/employee")
    public List<EmployeeResponse> getDetailEmployee(){
        return employeeRepository.getJoin();
    }
}
