package com.example.demo.dto;


import com.example.demo.entity.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRequest {
    private Employee employee;

    public Employee getEmployee(){
        return employee;
    }
}
