package org.example;

public class ArrayMulti {
    public void showArray(){
        String[] string1 = {"Q", "W", "E", "R"};
        String[][] string2 = {
                {"Q", "W", "E", "R"},
                {"A", "B", "C", "D"}
        };

        for (int i = 0; i<string1.length; i++){
            System.out.println(string1[i]);
        }
        for (int i = 0; i<string2.length; i++){
            System.out.println(string2[i]);
        }

    }
}
