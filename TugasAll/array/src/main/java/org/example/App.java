package org.example;

import java.util.ArrayList;
import java.util.Iterator;

    public class App
    {
        public static void main( String[] args )
        {
            ArrayList<String> list = new ArrayList<>();
            list.add("A");
            list.add("B");
            list.add("C");
            list.add("D");

            ArrayList<String> listNew = new ArrayList<>();
            listNew.add("D");
            listNew.add("E");

            ArrayList<String> duplicateElement = new ArrayList<>();
            duplicateElement.addAll(list);

            duplicateElement.retainAll(listNew);

            list.addAll(listNew);
            list.removeAll(duplicateElement);

            System.out.println("Hasil");
            for (String namelist : list) {
                System.out.println(namelist);
            }
        }
    }
