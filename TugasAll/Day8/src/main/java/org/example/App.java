package org.example;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
//        System.out.println("Hello World");
//        TryTest newThrow = new TryTest();
//        newThrow.test();

        String[] days = {" ","M ", "S ", "S ", "R ", "K ", "J ", "S "};

        //januari
//        Calendar cal = Calendar.getInstance();
        LocalDate thisMonth = LocalDate.of(2020, 2,1);
        DateTimeFormatter newParser = DateTimeFormatter.ofPattern("c");


//        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
//        LocalDate startDay = today.withDayOfMonth(today.lengthOfMonth());
//        System.out.println(startDay);
//        System.out.println(cal.getTime());
        //input
        int startDay = Integer.valueOf(thisMonth.format(newParser));
        int endDate  = thisMonth.lengthOfMonth();
        int sisaHari = 7 - ((startDay-1 + endDate) %7);
        System.out.println("Hari dimulai = " + startDay);
        System.out.println("Hari kelar = " + endDate);

        //
        System.out.println("=======Bulan=======");
        for (int i = 1; i < days.length; i++) {
            System.out.print(days[i] + " ");
        }

        System.out.println();

        //buat nambah garis diawal
        for (int i = 1; i < startDay; i++){
            System.out.print("-- ");
        }
        //Output Kalender
        for (int i = 1; i <= endDate; i++) {
            System.out.print(i + " ");
            if (i < 10) System.out.print(" ");

            if ((i+startDay-1) % 7 == 0) System.out.println();
        }
        //Nambah garis di akhir
        if(sisaHari !=7) {
            for (int i = 1; i <= sisaHari; i++) {
                System.out.print("-- ");
            }
        }

        System.out.println("=======Minggu======");

        int showWeek = 3;
        int startWeek = 1;

        if(showWeek==startWeek) {
            for (int i = 1; i < startDay; i++) {
                System.out.print("-- ");
            }
        }

        for (int i = 0; i <= endDate; i++) {
            if (startWeek == showWeek) {
                System.out.print(i + " ");
                if (i < 10) System.out.print(" ");
            }
            if ((i+startDay-1) % 7 == 0) {
                if(startWeek == showWeek)
                    System.out.println();
                startWeek++;
            }
        }

        if(showWeek == startWeek){
            int lastDash = 7 - ((startDay - 1 + endDate) % 7);
            if(lastDash < 6)
                for (int i = 0; i < lastDash ; i++) {
                    System.out.println("-- ");
                }
        }
    }
}
