package org.example;

import javafx.scene.chart.ScatterChart;

import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {

        try {
            Socket s = new Socket("192.168.43.119", 6666);

            DataInputStream din = new DataInputStream(s.getInputStream());
            DataOutputStream dout = new DataOutputStream(s.getOutputStream());

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String user = "Client : ";
            String msgin = "";
            String msgout = "";

            while (true) {
                msgout = br.readLine();
                dout.writeUTF(user + msgout);
                msgin = din.readUTF();
                System.out.println(msgin);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
